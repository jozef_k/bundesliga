import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import repository.IClubRepository;
import repository.IGameRepository;
import repository.IPlayerRepository;
import repository.IRepositoryCatalog;
import repository.impl.DummyRepositoryCatalog;
import repository.impl.mySqlImpl.ClubBuilder;
import repository.impl.mySqlImpl.ClubRepository;
import repository.impl.mySqlImpl.GameBuilder;
import repository.impl.mySqlImpl.GameRepository;
import repository.impl.mySqlImpl.PlayerBuilder;
import repository.impl.mySqlImpl.PlayerRepository;
import unitofwork.UnitOfWork;
import unitofwork.IUnitOfWork;

import domain.*;

public class Main {

	public static void main(String[] args) {
		String user = "root";
		String password = "";

		String url = "jdbc:mysql://localhost:3306/java";

		Club bayern = new Club();
		bayern.setName("FC Bayern Munchen");
		bayern.setStadium("Allianz Arena");
		
		Club schalke04 = new Club();
		schalke04.setName("Schalke 04");
		
		Game bayernVSschalke04 = new Game();
		bayernVSschalke04.setRound(1);
		bayernVSschalke04.setDate("25.10.2014");
		bayernVSschalke04.setClubHomeId(1);
		bayernVSschalke04.setClubAwayId(2);

		Player franckRibery = new Player();
		franckRibery.setFirstName("Franck");
		franckRibery.setSurname("Ribery");
		franckRibery.setPosition("pomocnik");
		franckRibery.setNr(7);
		franckRibery.setClubId(1);
				
		Player julianDraxler = new Player();
		julianDraxler.setFirstName("Julian");
		julianDraxler.setSurname("Draxler");
		julianDraxler.setPosition("pomocnik");
		julianDraxler.setNr(10);
		julianDraxler.setClubId(2);
		
		Player arjenRobben = new Player();
		arjenRobben.setFirstName("Arjen");
		arjenRobben.setSurname("Robben");
		arjenRobben.setPosition("pomocnik");
		arjenRobben.setNr(10);
		arjenRobben.setClubId(1);

/*	
		bayern.getPlayers().add(franckRibery);
		schalke04.getPlayers().add(julianDraxler);
		bayern.getPlayers().add(arjenRobben);
		
		IRepositoryCatalog catalog = new DummyRepositoryCatalog();
		
		catalog.getSchedules().add(r1);
		
		catalog.getPlayers().add(julianDraxler);
		catalog.getPlayers().add(arjenRobben);
		catalog.getPlayers().add(franckRibery);
		
		catalog.getClubs().add(bayern);
		catalog.getClubs().add(schalke04);
*/
		
		try {
			Connection connection = DriverManager.getConnection(url, user, password);
			
			UnitOfWork uow = new UnitOfWork(connection);
			
			String createTablePlayers = "CREATE TABLE players (id int AUTO_INCREMENT PRIMARY KEY, firstName VARCHAR(25), surname VARCHAR(50), position VARCHAR(10), nr VARCHAR(2), clubId INT)";
			String createTableClubs = "CREATE TABLE clubs (id int AUTO_INCREMENT PRIMARY KEY, name VARCHAR(25), stadium VARCHAR(50), budget VARCHAR(10))";
			String createTableGames = "CREATE TABLE games (id int AUTO_INCREMENT PRIMARY KEY, round INT, date VARCHAR(25), clubHomeId INT, clubAwayId INT)";
			
			Statement stmt = connection.createStatement();	
			stmt.executeUpdate(createTablePlayers);
			stmt.executeUpdate(createTableClubs);
			stmt.executeUpdate(createTableGames);
			
			IClubRepository clubs = new ClubRepository(connection, new ClubBuilder(), uow);
			clubs.add(bayern);
			clubs.add(schalke04);
			
			IPlayerRepository players = new PlayerRepository(connection, new PlayerBuilder(), uow);
			players.add(franckRibery);
			players.add(julianDraxler);
			players.add(arjenRobben);
			
			IGameRepository games = new GameRepository(connection, new GameBuilder(), uow);
			games.add(bayernVSschalke04);

			uow.commit();
			
			Club leverkusen = new Club();
			leverkusen.setName("Bayer 04 Leverkusen");
 			clubs.add(leverkusen);
 			
 			uow.commit();
 			
 			System.out.println("Kluby:");
			List<Club> clubsAll = new ArrayList<Club>();
			clubsAll = clubs.getAll();
			for(Club c: clubsAll) {
				System.out.println(c.getName());
			}
 			
 			Club fcb = new Club();
 			fcb = clubs.get(1);
 			fcb.setName("FC Bayern München");
 			clubs.update(fcb);
 			
 			uow.commit();
			
			System.out.println("Kluby:");
			List<Club> clubsAllNew = new ArrayList<Club>();
			clubsAllNew = clubs.getAll();
			for(Club c: clubsAllNew) {
				System.out.println(c.getName());
			}
			
			/*
			System.out.println();
			
			System.out.println("Piłkarze FC Bayern Munchen:");
			List<Player> playersByClub = new ArrayList<Player>();
			playersByClub = players.byClubId(1);
			for(Player p: playersByClub) {
				System.out.println(p.getNr() + ". " + p.getFirstName() + " " + p.getSurname());
			}
			*/
			
			String dropTablePlayers = "DROP TABLE players";
			String dropTableClubs = "DROP TABLE clubs";
			String dropTableGames = "DROP TABLE games";
			
			stmt.executeUpdate(dropTablePlayers);
			stmt.executeUpdate(dropTableClubs);
			stmt.executeUpdate(dropTableGames);
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
		//System.out.println("Koniec!");
	
/*	
		System.out.println("SKŁADY\n");
		
		for(Club c : catalog.getClubs().getAll()) {
			System.out.println(c.getName().toUpperCase() + ":");
			
			for(Player p : catalog.getPlayers().byClub(c)) {
				System.out.println(p.getNr() + ". " + p.getFirstName() + " " + p.getSurname());
			}
			
			System.out.println();
		}
		
		System.out.println("TERMINARZ\n");
		
		for(Schedule s : catalog.getSchedules().getAll()) {
			System.out.println(s.getRound() + ". kolejka:");	
			
			for(Game g : catalog.getGames().bySchedule(s)) {
				System.out.print(g.getDate() + ", "); //date
				System.out.print(catalog.getClubs().get(0).getStadium() + " | "); //stadium
				System.out.print(catalog.getClubs().get(0).getName()); //home team
				System.out.print(" - ");
				System.out.print(catalog.getClubs().getAll().get(catalog.getClubs().getAll().size() - 1).getName()); //guest team
			}
			
			System.out.println();
		}
*/
	}
}
