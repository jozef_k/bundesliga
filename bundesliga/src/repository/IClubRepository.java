package repository;

import java.util.List;

import domain.Club;

public interface IClubRepository extends IRepository<Club> {

}
