package repository;

import java.util.List;

import domain.Player;
import domain.Club;

public interface IPlayerRepository extends IRepository<Player> {
	public List<Player> byClubId(int clubId);
}
