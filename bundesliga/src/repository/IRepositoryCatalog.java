package repository;

import domain.*;

public interface IRepositoryCatalog {
	public IPlayerRepository getPlayers();
	public IClubRepository getClubs();
	public IGameRepository getGames();
}
	