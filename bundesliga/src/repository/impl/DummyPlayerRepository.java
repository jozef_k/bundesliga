package repository.impl;

import java.util.ArrayList;
import java.util.List;

import domain.Player;
import domain.Club;

import repository.IPlayerRepository;

public class DummyPlayerRepository implements IPlayerRepository {
	private DummyDb db;
	
	public DummyPlayerRepository(DummyDb db) {
		super();
		this.db = db;
	}
		
	public Player get(int id) {
		for(Player p : db.players) {
			if(p.getId()==id)
			return p;
		}
		
		return null;
	}
	
	public List<Player> getAll() {
		return db.players;
	}
	
	public void add(Player entity) {
		db.players.add(entity);
	}
	
	public void delete(Player entity) {
		db.players.remove(entity);
	}
	
	public void update(Player entity) {
		
	}
	
	public List<Player> byClubId(int clubId) {
		ArrayList<Player> players = new ArrayList<Player>();
		
		for(Player p : db.players) {
			if(p.getClubId() == clubId) {
				players.add(p);
			}
		}
		
		return players;

	}
}
