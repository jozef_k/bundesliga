package repository.impl;

import java.util.*;
import domain.*;

public class DummyDb {
	public List<Player> players = new ArrayList<Player>();
	public List<Club> clubs = new ArrayList<Club>();
	public List<Game> games = new ArrayList<Game>();
}
