package repository.impl;

import java.util.List;

import domain.Club;
import repository.IClubRepository;

public class DummyClubRepository implements IClubRepository {
	private DummyDb db;
	
	public DummyClubRepository(DummyDb db) {
		super();
		this.db = db;
	}
	
	public Club get(int id) {
		for(Club c : db.clubs) {
			if(c.getId() == id)
			return c;
		}
		
		return null;
	}
	
	public List<Club> getAll() {
		return db.clubs;
	}
	
	public void add(Club entity) {
		db.clubs.add(entity);	
	}

	public void delete(Club entity) {

	}
	
	public void update(Club entity) {
		
	}
}
