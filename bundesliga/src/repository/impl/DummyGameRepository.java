package repository.impl;

import java.util.ArrayList;
import java.util.List;

import domain.Game;

import repository.IGameRepository;

public class DummyGameRepository implements IGameRepository {
	private DummyDb db;
	
	public DummyGameRepository(DummyDb db) {
		super();
		this.db = db;
	}
	
	public Game get(int id) {
		for(Game g : db.games) {
			if(g.getId() == id)
			return g;
		}
		
		return null;
	}
	
	public List<Game> getAll() {
		return db.games;
	}
	
	public void add(Game entity) {
		db.games.add(entity);	
	}

	public void delete(Game entity) {

	}
	
	public void update(Game entity) {
		
	}
}
