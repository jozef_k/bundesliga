package repository.impl;

import repository.IRepository;
import repository.IRepositoryCatalog;
import repository.IPlayerRepository;
import repository.IClubRepository;
import repository.IGameRepository;
	
import domain.*;

public class DummyRepositoryCatalog implements IRepositoryCatalog {
	DummyDb db = new DummyDb();

	public IPlayerRepository getPlayers() {
		return new DummyPlayerRepository(db);
	}
	
	public IClubRepository getClubs() {
		return new DummyClubRepository(db);
	}
	
	public IGameRepository getGames() {
		return new DummyGameRepository(db);
	}
}
