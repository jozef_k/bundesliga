package repository.impl.mySqlImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import domain.Game;

public class GameBuilder implements IEntityBuilder<Game> {

	public Game build(ResultSet rs) throws SQLException {
		Game game = new Game();
		game.setId(rs.getInt("id"));
		game.setRound(rs.getInt("round"));
		game.setDate(rs.getString("date"));
		game.setClubHomeId(rs.getInt("clubHomeId"));
		game.setClubAwayId(rs.getInt("clubAwayId"));
		return game;
	}
}