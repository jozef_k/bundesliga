package repository.impl.mySqlImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import domain.Player;

public class PlayerBuilder implements IEntityBuilder<Player> {

	public Player build(ResultSet rs) throws SQLException {
		Player player = new Player();
		player.setId(rs.getInt("id"));
		player.setFirstName(rs.getString("firstName"));
		player.setSurname(rs.getString("surname"));
		player.setPosition(rs.getString("position"));
		player.setNr(rs.getInt("nr"));
		player.setClubId(rs.getInt("clubId"));
		return player;
	}
}