package repository.impl.mySqlImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import domain.*;
import repository.IPlayerRepository;
import unitofwork.IUnitOfWork;

public class PlayerRepository extends Repository<Player> implements IPlayerRepository {
	
	public PlayerRepository(Connection connection, IEntityBuilder<Player> builder, IUnitOfWork uow) {
		super(connection, builder, uow);
	}
	
	@Override
	protected String getTableName() {
		return "clubs";
	}
	
	@Override
	protected String getInsertQuery() {
		return "INSERT INTO players (firstName, surname, position, nr, clubId) VALUES (?, ?, ?, ?, ?)";
	}

	@Override
	protected String getUpdateQuery() {
		return "UPDATE players SET firstName=?, surname=?, position=?, nr=?, clubId=? WHERE id=?";
	}
	
	@Override
	protected void setUpInsertQuery(Player entity) throws SQLException {
		insert.setString(1, entity.getFirstName());
		insert.setString(2, entity.getSurname());
		insert.setString(3, entity.getPosition());
		insert.setInt(4, entity.getNr());
		insert.setInt(5, entity.getClubId());
	}

	@Override
	protected void setUpUpdateQuery(Player entity) throws SQLException {
		update.setString(1, entity.getFirstName());
		update.setString(2, entity.getSurname());
		update.setString(3, entity.getPosition());
		update.setInt(4, entity.getNr());
		update.setInt(5, entity.getClubId());
		update.setInt(6, entity.getId());
	}
	
	public List<Player> getAll() {

		List<Player> result = new ArrayList<Player>();
		
		try {
			ResultSet rs = selectAll.executeQuery();
			
			while(rs.next()) {
				Player player = new Player();
				player.setId(rs.getInt("id"));
				player.setFirstName(rs.getString("firstName"));
				player.setSurname(rs.getString("surname"));
				player.setPosition(rs.getString("position"));
				player.setNr(rs.getInt("nr"));
				player.setClubId(rs.getInt("clubId"));
				result.add(player);
			}	
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return result;
	}

	public List<Player> byClubId(int clubId) {
		// TODO Auto-generated method stub
		return null;
	}
}
