package repository.impl.mySqlImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

import domain.Club;

public class ClubBuilder implements IEntityBuilder<Club> {

	public Club build(ResultSet rs) throws SQLException {
		Club club = new Club();
		club.setId(rs.getInt("id"));
		club.setName(rs.getString("name"));
		club.setStadium(rs.getString("stadium"));
		club.setBudget(rs.getDouble("budget"));
		return club;
	}
}
