package repository.impl.mySqlImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import domain.*;
import repository.IClubRepository;
import unitofwork.IUnitOfWork;

public class ClubRepository extends Repository<Club> implements IClubRepository {
	
	public ClubRepository(Connection connection, IEntityBuilder<Club> builder, IUnitOfWork uow) {
		super(connection, builder, uow);
	}

	@Override
	protected String getTableName() {
		return "clubs";
	}
	
	@Override
	protected String getInsertQuery() {
		return "INSERT INTO clubs (name, stadium, budget) VALUES (?,?,?)";
	}

	@Override
	protected String getUpdateQuery() {
		return "UPDATE clubs SET name=?, stadium=?, budget=? WHERE id=?";
	}
	
	@Override
	protected void setUpInsertQuery(Club entity) throws SQLException {
		insert.setString(1, entity.getName());
		insert.setString(2, entity.getStadium());
		insert.setDouble(3, entity.getBudget());
	}

	@Override
	protected void setUpUpdateQuery(Club entity) throws SQLException {
		update.setString(1, entity.getName());
		update.setString(2, entity.getStadium());
		update.setDouble(3, entity.getBudget());
		update.setInt(4, entity.getId());
	}
}
