package repository.impl.mySqlImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import domain.*;
import repository.IGameRepository;
import unitofwork.IUnitOfWork;

public class GameRepository extends Repository<Game> implements IGameRepository {
	
	public GameRepository(Connection connection, IEntityBuilder<Game> builder, IUnitOfWork uow) {
		super(connection, builder, uow);
	}
	
	@Override
	protected String getTableName() {
		return "games";
	}
	
	@Override
	protected String getInsertQuery() {
		return "INSERT INTO games (round, date, clubHomeId, clubAwayId) VALUES (?, ?, ?, ?)";
	}

	@Override
	protected String getUpdateQuery() {
		return "UPDATE games SET round=?, date=?, clubHomeId=?, clubAwayId=? WHERE id=?";
	}
	
	@Override
	protected void setUpInsertQuery(Game entity) throws SQLException {
		insert.setInt(1, entity.getRound());
		insert.setString(2, entity.getDate());
		insert.setInt(3, entity.getClubHomeId());
		insert.setInt(4, entity.getClubAwayId());
	}

	@Override
	protected void setUpUpdateQuery(Game entity) throws SQLException {
		update.setInt(1, entity.getRound());
		update.setString(2, entity.getDate());
		update.setInt(3, entity.getClubHomeId());
		update.setInt(4, entity.getClubAwayId());
		update.setInt(5, entity.getId());
	}

	public List<Game> getAll() {

		List<Game> result = new ArrayList<Game>();
		
		try {
			ResultSet rs = selectAll.executeQuery();
			
			while(rs.next()) {
				Game game = new Game();
				game.setId(rs.getInt("id"));
				game.setRound(rs.getInt("round"));
				game.setDate(rs.getString("date"));
				game.setClubHomeId(rs.getInt("clubHomeId"));
				game.setClubAwayId(rs.getInt("clubAwayId"));
				result.add(game);
			}	
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return result;
	}
}
