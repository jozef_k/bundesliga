package domain;

import java.util.ArrayList;
import java.util.List;

public class Game extends Entity {
	private int round;
	private String date;
	private int clubHomeId;
	private int clubAwayId;
	
	public int getRound() {
		return round;
	}
	public void setRound(int round) {
		this.round = round;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getClubHomeId() {
		return clubHomeId;
	}
	public void setClubHomeId(int clubHomeId) {
		this.clubHomeId = clubHomeId;
	}
	public int getClubAwayId() {
		return clubAwayId;
	}
	public void setClubAwayId(int clubAwayId) {
		this.clubAwayId = clubAwayId;
	}

}
