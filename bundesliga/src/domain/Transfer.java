package domain;

public class Transfer extends Entity {
	private int playerId;
	private int fromClubId;
	private int toClubId;
	private float cost;
	
	public int getPlayerId() {
		return playerId;
	}
	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}
	public int getFromClubId() {
		return fromClubId;
	}
	public void setFromClubId(int fromClubId) {
		this.fromClubId = fromClubId;
	}
	public int getToClubId() {
		return toClubId;
	}
	public void setToClubId(int toClubId) {
		this.toClubId = toClubId;
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		this.cost = cost;
	}
}
