package domain;

public class Goal extends Entity {
	private int gameId;
	private int minute;
	private int playerId;
	private int ownGoal;
	
	public int getGameId() {
		return gameId;
	}
	public void setGameId(int gameId) {
		this.gameId = gameId;
	}
	public int getMinute() {
		return minute;
	}
	public void setMinute(int minute) {
		this.minute = minute;
	}
	public int getPlayerId() {
		return playerId;
	}
	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}
	public int getOwnGoal() {
		return ownGoal;
	}
	public void setOwnGoal(int ownGoal) {
		this.ownGoal = ownGoal;
	}
}
