package domain;

import java.util.ArrayList;
import java.util.List;

public class Player extends Entity {
	private String firstName;
	private String surname;
	private String position;
	private int nr;
	private int clubId;
/*
	private List<Goal> goals;
	
	public Player() {
		goals = new ArrayList<Goal>();
		this.firstName = "robert";
	}
*/
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	/*
	public List<Goal> getGoals() {
		return goals;
	}
	public void setGoals(List<Goal> goals) {
		this.goals = goals;
	}
	*/
	public int getNr() {
		return nr;
	}
	public void setNr(int nr) {
		this.nr = nr;
	}
	public int getClubId() {
		return clubId;
	}
	public void setClubId(int clubId) {
		this.clubId = clubId;
	}
	
	@Override
	public String toString() {
		return "Player [firstName=" + firstName + ", surname=" + surname
				+ ", position=" + position + ", nr=" + nr
				+ ", clubId=" + clubId + "]";
	}
}
