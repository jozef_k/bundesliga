package domain;

import java.util.ArrayList;
import java.util.List;

public class Club extends Entity {
	
	private String name;
	private String stadium;
	private double budget;
	
	private List<Player> players;
	
	public Club() {
		players = new ArrayList<Player>();
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getBudget() {
		return budget;
	}
	public void setBudget(double budget) {
		this.budget = budget;
	}
	public List<Player> getPlayers() {
		return players;
	}
	public void setPlayers(List<Player> players) {
		this.players = players;
	}
	public String getStadium() {
		return stadium;
	}
	public void setStadium(String stadium) {
		this.stadium = stadium;
	}
}
